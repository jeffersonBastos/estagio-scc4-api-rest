package com.SCC4.apirest.resources;

import java.text.Normalizer;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PrincipalCNTRL {
	
	
	@GetMapping(path= "/listaReversa")
    public ResponseEntity<String> listaReversa(@RequestParam String lista) {
				
		String[] listaArr = lista.split(",");
		int tamanho = listaArr.length;
		String[] arrayInvertido = new String[tamanho];
		String listaInvertida = "";
		
		for (int i = 0; i < arrayInvertido.length; i++) {
			tamanho -= 1 ;
			arrayInvertido[i] = listaArr[tamanho];
		}		
		
		listaInvertida = arrayStringToString(arrayInvertido);
		
		return ResponseEntity.status(HttpStatus.OK).body(listaInvertida);
    }
    
	
	@GetMapping(path= "/imprimirImpares")
    public ResponseEntity<String> imprimirImpares(@RequestParam String lista) {
		try {
			int[] listaArrInt = stringToArrayInt(lista);
			String listaImpares = "";
			
			for(int i=0;i < listaArrInt.length;i++) {
				
				if(!Par(listaArrInt[i])) {
					listaImpares += String.valueOf(listaArrInt[i]) +" ";
				}
			}
	        return ResponseEntity.status(HttpStatus.OK).body(listaImpares);
		}    
	    catch (NumberFormatException e) {
	        return ResponseEntity.status(HttpStatus.OK).body("Valor em um formato invalido");

		}
    }
	
	
	@GetMapping(path= "/imprimirPares")
    public ResponseEntity<String> imprimirPares(@RequestParam String lista) {
		try {
			int[] listaArrInt = stringToArrayInt(lista);
			String listaPares = "";
			
			for(int i=0;i < listaArrInt.length;i++) {
				
				if(Par(listaArrInt[i])) {
					listaPares += String.valueOf(listaArrInt[i]) +" ";
				}
			}
	        return ResponseEntity.status(HttpStatus.OK).body(listaPares);
		}
		catch (NumberFormatException e) {
	        return ResponseEntity.status(HttpStatus.OK).body("Valor em um formato invalido");

		}
    }
	
	@GetMapping(path= "/tamanho")
    public ResponseEntity<String> tamanho(@RequestParam String palavra) {
		int n = palavra.length();
		String tamanho = "tamanho = " + n;
        
		return ResponseEntity.status(HttpStatus.OK).body(tamanho);
    }

	
	@GetMapping(path= "/maiusculas")
    public ResponseEntity<String> maiusculas(@RequestParam String palavra) {
		String maiusculas = palavra.toUpperCase();
        
		return ResponseEntity.status(HttpStatus.OK).body(maiusculas);

    }
	
	
	@GetMapping(path= "/vogais")
    public ResponseEntity<String> vogais(@RequestParam String palavra) {
		
		palavra = Normalizer.normalize(palavra, Normalizer.Form.NFD);
        palavra = palavra.replaceAll("[^\\p{ASCII}]", "");
        
        palavra = palavra.replaceAll("[^aeiouAEIOU]", "");
        
		return ResponseEntity.status(HttpStatus.OK).body(palavra);
    }	
	
	
	@GetMapping(path= "/consoantes")
    public ResponseEntity<String> consoantes(@RequestParam String palavra) {

		palavra = Normalizer.normalize(palavra, Normalizer.Form.NFD);
        palavra = palavra.replaceAll("[^\\p{ASCII}]", "");
        
        palavra = palavra.replaceAll("[^A-Za-z]", "");
        palavra = palavra.replaceAll("[aeiouAEIOU]", "");

		return ResponseEntity.status(HttpStatus.OK).body(palavra);
    }	
	
	
	@GetMapping(path= "/nomeBibliografico")
    public ResponseEntity<String> nomeBibliografico(@RequestParam String nome) {
		String[] arrNome = nome.split("%");
		int tamanho = arrNome.length;
		String nomeFinal ="";
		if(tamanho > 1) {
			nomeFinal = arrNome[tamanho-1] + ", ";
			for(int i=0;i<arrNome.length - 1;i++) {
				nomeFinal += arrNome[i] + " ";
			}
		}else {
			nomeFinal = arrNome[0];
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(nomeFinal);
    }	
	
	
	@GetMapping(path= "/sistemaMonetario")	
	public ResponseEntity<String> sistemaMonetario(@RequestParam String total) {		
		
		String strUltimoDigito = "";
		long valor = parseLong(total);
		int nota5 = 0;
		int nota3 = 0;
		
		if(valor == -1){
			return ResponseEntity.status(HttpStatus.OK).body("Valor em um formato invalido");
		}else{ 
			if(valor<3||valor==4||valor==7) {
				return ResponseEntity.status(HttpStatus.OK).body("valor invalido, o valor deve ser inteiro maior que 2 diferente de 4 e 7");
			}else{
				if(valor % 5 == 0) {
					nota5 = (int) (valor / 5);
					
				}else {
					strUltimoDigito = total.substring (total.length() - 1);
					
					switch (Integer.parseInt(strUltimoDigito)) {
					case 1:
						nota3 = 2;
						nota5 = (int) ((valor - 6) / 5);
						break;
					case 2:
						nota3 = 4;
						nota5 = (int) ((valor - 12) / 5);
						break;
					case 3:
						nota3 = 1;
						nota5 = (int) ((valor - 3) / 5);
						break;
					case 4:
						nota3 = 3;
						nota5 = (int) ((valor - 9) / 5);
						break;
					case 6:
						nota3 = 2;
						nota5 = (int) ((valor - 6) / 5);
						break;
					case 7:
						nota3 = 4;
						nota5 = (int) ((valor - 12) / 5);
						break;
					case 8:
						nota3 = 1;
						nota5 = (int) ((valor - 3) / 5);
						break;
					case 9:
						nota3 = 3;
						nota5 = (int) ((valor - 9) / 5);
						break;
					default:
						return ResponseEntity.status(HttpStatus.OK).body("Não foi possivel concluir sua operão");
					}					
				}			
			}
		}
		
		return ResponseEntity.status(HttpStatus.OK).body("Saque R$" + total + ": " + nota3 + " nota(s) de R$3 e "+ nota5 + " nota(s) de R$5 ");
	}
	
    
	private long parseLong(String valorString) {
    	try {
			return Long.parseLong(valorString);

		} catch (NumberFormatException e) {
			return  -1;

		} 
	}

	private Boolean Par(int numero) {
    	return(numero % 2 == 0);   
    }
    
    private String arrayStringToString(String[] arr) {
    	String string ="";
    	for (int i = 0; i < arr.length ; i++) { 			
    		if(i == arr.length-1) {
				string += arr[i];
			}else {
				string += arr[i]+",";
			}			
		}
    	return string;
    }
    
	private int[] stringToArrayInt (String lista) {
		
		String[] strings = lista.split(",");
		int[] ints = new int[strings.length];

		for(int i = 0; i < strings.length; ++i){
		  ints[ i ] = Integer.parseInt(strings[ i ]);
		}
		return ints;
	}
	
	
}
